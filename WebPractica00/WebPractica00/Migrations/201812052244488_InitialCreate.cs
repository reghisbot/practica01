namespace WebPractica00.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgrupadorDoms",
                c => new
                    {
                        AgrupadorDomId = c.Int(nullable: false, identity: true),
                        DomicilioId = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AgrupadorDomId);
            
            CreateTable(
                "dbo.Ciudads",
                c => new
                    {
                        CiudadId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.CiudadId);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellido = c.String(),
                    })
                .PrimaryKey(t => t.ClienteId);
            
            CreateTable(
                "dbo.Domicilios",
                c => new
                    {
                        DomicilioId = c.Int(nullable: false, identity: true),
                        CiudadId = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                        Calle = c.String(),
                        Altura = c.Int(nullable: false),
                        Piso = c.Int(nullable: false),
                        Depto = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DomicilioId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Domicilios");
            DropTable("dbo.Clientes");
            DropTable("dbo.Ciudads");
            DropTable("dbo.AgrupadorDoms");
        }
    }
}
