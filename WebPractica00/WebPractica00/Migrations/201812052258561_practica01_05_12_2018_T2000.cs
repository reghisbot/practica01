namespace WebPractica00.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class practica01_05_12_2018_T2000 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ciudads", "Descripcion", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Clientes", "Nombre", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Clientes", "Apellido", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Domicilios", "Calle", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Domicilios", "Calle", c => c.String());
            AlterColumn("dbo.Clientes", "Apellido", c => c.String());
            AlterColumn("dbo.Clientes", "Nombre", c => c.String());
            AlterColumn("dbo.Ciudads", "Descripcion", c => c.String());
        }
    }
}
