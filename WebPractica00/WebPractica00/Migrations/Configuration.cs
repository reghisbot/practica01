namespace WebPractica00.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebPractica00.Contexto;
    using WebPractica00.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebPractica00.Contexto.Practica00Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Practica00Context context)
        {
            //  This method will be called after migrating to the latest version.
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            var _ciudades = new List<Ciudad>
            {
                new Ciudad {  CiudadId = 100, Descripcion = "Argentina" },
                new Ciudad {  CiudadId = 200, Descripcion = "Bolivia" },
                new Ciudad {  CiudadId = 300, Descripcion = "Brasil" },
                new Ciudad {  CiudadId = 400, Descripcion = "Mexico" },
                new Ciudad {  CiudadId = 500, Descripcion = "Peru" }
            };
            _ciudades.ForEach(
                    s => context.Ciudads.AddOrUpdate
                    (   
                        p => p.Descripcion, s)
                    );
            context.SaveChanges();
        }
    }
}
