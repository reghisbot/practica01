﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPractica00.Models;

namespace WebPractica00.Contexto
{
    public class Practica00Context : DbContext
    {
        public Practica00Context() : base("DefaultConnection") 
        {
        }

        public System.Data.Entity.DbSet<WebPractica00.Models.Ciudad> Ciudads { get; set; }

        public System.Data.Entity.DbSet<WebPractica00.Models.Cliente> Clientes { get; set; }

        public System.Data.Entity.DbSet<WebPractica00.Models.Domicilio> Domicilios { get; set; }

    }
}