﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPractica00.Models
{
    
    public class Cliente
    {
        [Key]
        public int ClienteId  { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(30, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public String Nombre { get; set; }
        
        [Display(Name = "Apellido")]
        [StringLength(30, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public String Apellido { get; set; }

        public int CiudadId { get; set; }   // puntero 
        // lado Muestra ciudades 
        public virtual Ciudad Ciudades { get; set; }

        
    }
}