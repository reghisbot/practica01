﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPractica00.Models
{
    
    public class Domicilio
    {
        [Key]
        public int DomicilioId { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public String Calle { get; set; }
        [Display(Name = "Altura")]
        [Required(ErrorMessage = "El campo {0} es requerido ")]  //Campo Obligatorio
        public int Altura { get; set; }
        public int Piso { get; set; }
        public int Depto { get; set; }


        public int CiudadId { get; set; }        // puntero
        // lado muestra la ciudad   
        public virtual Ciudad Ciudades { get; set; }


    }
}