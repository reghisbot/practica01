﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPractica00.Models
{
    public class AgrupadorDom 
    {

        [Key]
        public int AgrupadorDomId { get; set; }

        public int ClienteId { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(30, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public String Nombre { get; set; }
        
        [Display(Name = "Apellido")]
        [StringLength(30, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public String Apellido { get; set; }
        
        public int DomicilioId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public String Calle { get; set; }

        [Display(Name = "Altura")]
        [Required(ErrorMessage = "El campo {0} es requerido ")]  //Campo Obligatorio

        public int Altura { get; set; }
        
        public int Piso { get; set; }
        
        public int Depto { get; set; }

        [Required(ErrorMessage="El campo {0} es requerido")]
        [Display(Name="Fecha de Alta")]
        [DisplayFormat(DataFormatString="{0:yyyy-MM-dd hh:mm tt}", ApplyFormatInEditMode = false)]
        public DateTime FechaAlta { get; set; }

        public int CiudadId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Descripción")]
        [StringLength(30, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public String Descripcion { get; set; }


    }
}