﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPractica00.Models
{
    public class Ciudad
    {
        [Key]
        public int CiudadId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Descripción")]
        [StringLength(30, ErrorMessage = "El Campo {0} debe tener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public String Descripcion { get; set; }

        //////////lado Lista una Ciudad para varios clientes
        public virtual ICollection<Cliente> Cliente { get; set; }
        ////////// lado Lista una Ciudad tiene varios domicilios
        public virtual ICollection<Domicilio> Domicilio { get; set; }
    }
}