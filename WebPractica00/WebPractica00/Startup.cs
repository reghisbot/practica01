﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebPractica00.Startup))]
namespace WebPractica00
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
